const
  fs = require('fs'),
  path = require('path')

function getSource (path) {
  return fs.readFileSync(
    path, 
    { encoding: 'utf-8' })
}

module.exports = {
  getSource
}

