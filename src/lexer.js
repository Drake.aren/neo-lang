const
  moo = require('moo')

function getTokens (tokenRef, source) {
  let 
    lexer = moo.compile(tokenRef),
    tokens = []
  
  lexer.reset(source)
  
  for (token of lexer) {
    tokens.push(token)
  }

  return tokens
}

module.exports = {
  getTokens,
}
