const
  path = require('path'),
  util = require('util')

const
  lexer = require('./lexer.js'),
  { getSource } = require('./io.js'),
  { parse } = require('./parser.js')

filePath = path.join(__dirname, '../example/test.nl')

let tokenRef = {
  integer: /[0-9]+/,
  string: /"(?:\\["\\]|[^\n"\\])*"/,
  function: ['map', 'inc', 'range', 'fn', 'print'],
  space: { match: /\s/, lineBreaks: true },
  keyword: /[a-zA-Z0-9\?\!\@\#\$\%\^\|\&\*\_\+\<\>\(\)\{\}\.]+/,
}

source = getSource(filePath)

if (!source) {
  console.error('source is null')
  console.log(source)
  process.exit()
}

tokens = lexer.getTokens(tokenRef, source)

if (!tokens) {
  console.error('tokens in undefined')
  process.exit()
}

if (tokens.length == 0) {
  console.error('tokens is empty')
  console.log(tokens)
  process.exit()
}

//tokens = tokens.filter(e =>
//  e.type != 'space' && e.type != 'newline')

parsed = parse(tokens)

console.log(
  util.inspect(
    parsed,
    {showHidden: false, depth: null}))
