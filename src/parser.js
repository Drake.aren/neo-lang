
function parse (tokens) {
  let c = 0;

  const peek = () => tokens[c];
  const consume = () => tokens[c++];

  const parseArgs = () => {
    return consume()
  }

  const parseFunc = () => {
    let node = {
      ...consume(),
      expr: []
    }
    while(peek()) {
      node.expr.push(parseExpr())
    }
    return node
  }

  const parseExpr = () => {
    if (peek().type == 'function') {
      return parseFunc()
    } else {
      return parseArgs()
    }
  }

  return parseExpr();
}

module.exports = {
  parse
}
